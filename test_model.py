# Import libraries
import spacy
import string
import pickle
import pandas as pd

from spacy import displacy
from spacy.lang.id.stop_words import STOP_WORDS

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC # Use Support Vector Machine
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

# Load spacy in Bahasa Indonesia
nlp_id = spacy.blank('id')

# Load test data
data_test = pd.read_csv('test_data_restaurant.tsv', sep = '\t', header=None)
columns_name = ['Review', 'Sentiment']
data_test.columns = columns_name

X_test = data_test['Review']
y_test = data_test['Sentiment']

# Define punctuation and stopwords 
punct = string.punctuation
stopwords = list(STOP_WORDS)

# Lemmatization, tokenization, stopwords removal, and punctuation removal
def text_data_cleaning(sentence):
    doc = nlp_id(sentence)
    
    tokens = []
    for token in doc:
        if token.lemma_ != "-PRON-":
            temp = token.lemma_.lower().strip()
        else:
            temp = token.lower_
        tokens.append(temp)
    
    cleaned_tokens = []
    for token in tokens:
        if token not in stopwords and token not in punct:
            cleaned_tokens.append(token)
    return cleaned_tokens

# Load built model
filename = 'finalized_model.sav'
loaded_model = pickle.load(open(filename, 'rb'))

# Get predictions
y_pred = loaded_model.predict(X_test)

# Compare predictions with actual labels and get the performance
print(classification_report(y_test, y_pred))