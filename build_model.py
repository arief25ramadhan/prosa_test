"""build_model

# Sentiment classification for restaurant reviews


"""

# Import libraries
import spacy
import string
import pickle
import pandas as pd

from spacy import displacy
from spacy.lang.id.stop_words import STOP_WORDS

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC # Use Support Vector Machine
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
from sklearn.neural_network import MLPClassifier

# Load spacy in Bahasa Indonesia
nlp_id = spacy.blank('id')

# Load Train data
data_train = pd.read_csv('train_data_restaurant.tsv', sep = '\t', header=None)
columns_name = ['Review', 'Sentiment']
data_train.columns = columns_name

X_train = data_train['Review']
y_train = data_train['Sentiment']


# Load Test data
data_test = pd.read_csv('test_data_restaurant.tsv', sep = '\t', header=None)
columns_name = ['Review', 'Sentiment']
data_test.columns = columns_name

X_test = data_test['Review']
y_test = data_test['Sentiment']


# Define punctuation and stopwords
punct = string.punctuation
stopwords = list(STOP_WORDS)

# Lemmatization, tokenization, stopwords removal, and punctuation removal
def text_data_cleaning(sentence):
    doc = nlp_id(sentence)
    
    tokens = []
    for token in doc:
        if token.lemma_ != "-PRON-":
            temp = token.lemma_.lower().strip()
        else:
            temp = token.lower_
        tokens.append(temp)
    
    cleaned_tokens = []
    for token in tokens:
        if token not in stopwords and token not in punct:
            cleaned_tokens.append(token)
    return cleaned_tokens


# Vectorization Feature Engineering (TF-IDF)
tfidf = TfidfVectorizer(tokenizer = text_data_cleaning)

# Build MLP classifier
classifier = MLPClassifier(hidden_layer_sizes=(64, 32, 16), max_iter=150, alpha=1e-4,
                           tol = 0.00001, solver='sgd', learning_rate_init=.1, verbose=1)

# Complete the pipeline
clf = Pipeline([('tfidf', tfidf), ('clf', classifier)])

# Train the model
history = clf.fit(X_train, y_train)
print("Training set score: %f" % clf.score(X_train, y_train))

# Save model
filename = 'finalized_model.sav'
pickle.dump(clf, open(filename, 'wb'))

# Test performance
y_pred = clf.predict(X_test)
print(classification_report(y_test, y_pred))
confusion_matrix(y_test, y_pred)

# Test with custom input
clf.predict(["Menyajikan masakan khas nusantara yang sarat rempah, segar, dan gurih"])